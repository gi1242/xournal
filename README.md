# Custom patches for Xournal

These are patches for [Xournal](http://xournal.sourceforge.net/) I wrote in
2009 in order to do the following:

1. Emergency save before crashing.

2. Allow customizing the ruling parameters

3. Invert the colors of the whole journal (useful if you write on black paper,
   and want to print, or later want white paper.)

I use them every day, so I will update them as new versions of Xournal are
released.

The `upstream` branch tracks the vanilla upstream sources. The
`esave-X-DATE` branches are my patches rebased to xournal version `X` with
patches up to `DATE`.
